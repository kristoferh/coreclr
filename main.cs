﻿using System;

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Runtime.InteropServices
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class NativeCallableAttribute : Attribute
    {
        public string EntryPoint;
        public CallingConvention CallingConvention;
        public NativeCallableAttribute() { }
    }
}


public class main
{
    [NativeCallable(EntryPoint = "func1", CallingConvention = CallingConvention.StdCall)]
    public static IntPtr _1()
    {
	    var str = "hello!";
	    var cArr = System.Text.Encoding.ASCII.GetBytes(str);
	    var pResult = Marshal.AllocHGlobal(cArr.Length);
	    Marshal.Copy(cArr, 0, pResult, cArr.Length);
	    return pResult; //caller free:s
    }
    [NativeCallable(EntryPoint = "func2", CallingConvention = CallingConvention.StdCall)]
    public static ushort _2()
    {
	return 0xAAAA;
    }
    [UnmanagedFunctionPointer(CallingConvention.StdCall)]
    public delegate uint functionPtr();
    
    [NativeCallable(EntryPoint = "func3", CallingConvention = CallingConvention.StdCall)]
    public static uint _3(IntPtr callMe)
    {
	functionPtr pPtr = (functionPtr) Marshal.GetDelegateForFunctionPointer(callMe, typeof(functionPtr));
	return pPtr();
    }
}
