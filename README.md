# coreclr

### lunix:

#### dependencies
* clang-3.9
* dotnetcore2
* libkrb5-dev

#### building and running

to build a SO:
```
$ make cpp
$ ./main
````
static linking:
```
$ make static
$ ./main_static
```

### windows:

#### building and running
build cpp.vcxproj using VS or msbuild.

build the shared object by starting a x64 VS command prompt and running:

```
dotnet publish /p:NativeLib=shared -r win-x64 -c release shared.csproj
```

copy the resulting dll in to the cpp.vcxproj output directory. Run main.exe


### static linking
```
dotnet publish /p=NativeLib=static -r win-x64 -c release shared_static.csproj
```
build cpp.vcxproj - ensure to target Static_Release
run main.exe