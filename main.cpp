#ifdef __linux__ 
#include <dlfcn.h>
#define SHARED_OBJECT "./shared.so"
#else
#include <windows.h>
#include <combaseapi.h>

#define SHARED_OBJECT "./shared.dll"
#endif

#include <stdio.h>
#include <stdlib.h>

#ifdef __linux__
#define SHARED_FN(handle, fn) dlsym(handle, fn)
#define _FREE(ptr) free(ptr)
#else
#define SHARED_FN(handle, fn) GetProcAddress(handle, fn)
#define _FREE(ptr) CoTaskMemFree(ptr)
#endif

#ifdef STATIC_LINKING
  #ifdef __cplusplus
    extern "C" {
  #endif
  char *func1();
  unsigned short func2();
  unsigned int func3(unsigned int (*)());
  #ifdef __cplusplus
    }
  #endif
#endif



int main(int argc, char **argv)
{

auto fnInvokeFromShared = +[]() -> unsigned int { printf("big whoop!\n"); return 3735928559; };

#ifndef STATIC_LINKING
  char *(*func1)();
  unsigned short(*func2)();
  unsigned int (*func3)(unsigned int (*)());
  
#ifdef __linux__
  void *handle = nullptr;
  handle = dlopen(SHARED_OBJECT, RTLD_LAZY);
  if(!handle)
  {
    printf("Failed to load %s: %s, bailing...\n", SHARED_OBJECT, dlerror());
    exit(1);
  }
  dlerror();
#else
  auto handle = LoadLibrary(SHARED_OBJECT);

  if (!handle) {
	  printf("failed to load dll %s", SHARED_OBJECT);
	  return -1;
  }

#endif
  
  func1 = (char* (*)()) SHARED_FN(handle, "_1");
  func2 = (unsigned short(*)()) SHARED_FN(handle, "_2");
  func3 = (unsigned int (*)(unsigned int (*)())) SHARED_FN(handle, "_3");

#endif
  
  printf("Invoking _1\n");
  char *pStr = func1();
  printf("_1 returned: %s\n", pStr);
  _FREE(pStr);
  
  printf("Invoking _2\n");
  printf("_2 returned 0x%04X\n", func2());

  printf("Invoking _3\n");
  auto nFunc3Result = func3(fnInvokeFromShared);
  printf("_3 returned 0x%08X\n", nFunc3Result);

  //func1();
  return 0;
}
