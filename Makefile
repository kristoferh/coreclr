CLANG++-FLAGS=-std=c++11 -ldl -lstdc++
NUGET_PATH=~/.nuget/packages/runtime.linux-x64.microsoft.dotnet.ilcompiler/

dll.so : main.cs
	dotnet publish /p:NativeLib=shared -r linux-x64 -c release shared.csproj && cp ./bin/release/netstandard2.0/linux-x64/publish/shared.so .
cpp: dll.so main.cpp
	clang++-3.9 $(CLANG++-FLAGS) main.cpp -o main
static_lib: main.cs
	dotnet publish /t:LinkNative /p:NativeLib=static -r linux-x64 -c release shared_static.csproj && cp ./bin/release/netstandard2.0/linux-x64/publish/shared_static.a libdotnet_static.a

bootstrap_libs: static_lib
	mkdir -p bootstrap && cp -R $(NUGET_PATH)$(shell ls $(NUGET_PATH) | sort -n | tail -1)/sdk/*.a ./bootstrap/ && cp -R $(NUGET_PATH)$(shell ls $(NUGET_PATH) | sort -n | tail -1)/framework/*.a ./bootstrap/ && cd ./bootstrap && ls System.*.a | xargs -I{} mv {} lib{}

static: bootstrap_libs main.cpp
	clang++-3.9 $(CLANG++-FLAGS) -DSTATIC_LINKING -o main_static main.cpp -ldotnet_static -L . -L ./bootstrap -lbootstrapperdll -lRuntime -lSystem.Private.CoreLib.Native -lSystem.Private.TypeLoader.Native -lSystem.Native -lpthread -lSystem.Globalization.Native


clean :
	rm -R bin
